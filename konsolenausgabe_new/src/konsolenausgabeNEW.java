// Aufgabe 1

public class konsolenausgabeNEW {
	public static void main(String[] args) {
		System.out.println("Hallo Welt");
		System.out.println("Satz 2");
//		print gibt den inhalt der klammer wieder, println ebenfalls, jedoch springt es danach in eine neue zeile

//	Aufgabe 2

		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***"); 

//	Aufgabe 3

		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;

		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);

//	Übung 2, Aufgabe 1

		String s = "*";

		System.out.printf("%6s\n", s + s);
		System.out.printf("%1s", s);
		System.out.printf("%9s\n", s);
		System.out.printf("%1s", s);
		System.out.printf("%9s\n", s);
		System.out.printf("%6s\n", s + s);

	}
}